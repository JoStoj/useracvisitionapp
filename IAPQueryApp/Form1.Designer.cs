﻿namespace IAPQueryApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.androidRadioButton = new System.Windows.Forms.RadioButton();
            this.Platform = new System.Windows.Forms.GroupBox();
            this.iosRadioButton = new System.Windows.Forms.RadioButton();
            this.button3 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.csvFile = new System.Windows.Forms.TextBox();
            this.playersFile = new System.Windows.Forms.TextBox();
            this.purchasesFile = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.commaRadioButton = new System.Windows.Forms.RadioButton();
            this.lineRadioButton = new System.Windows.Forms.RadioButton();
            this.semicolonRadioButton = new System.Windows.Forms.RadioButton();
            this.fbRadioButton = new System.Windows.Forms.RadioButton();
            this.Platform.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(377, 275);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Start);
            // 
            // androidRadioButton
            // 
            this.androidRadioButton.AutoSize = true;
            this.androidRadioButton.Checked = true;
            this.androidRadioButton.Location = new System.Drawing.Point(6, 19);
            this.androidRadioButton.Name = "androidRadioButton";
            this.androidRadioButton.Size = new System.Drawing.Size(61, 17);
            this.androidRadioButton.TabIndex = 2;
            this.androidRadioButton.TabStop = true;
            this.androidRadioButton.Text = "Android";
            this.androidRadioButton.UseVisualStyleBackColor = true;
            this.androidRadioButton.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // Platform
            // 
            this.Platform.Controls.Add(this.fbRadioButton);
            this.Platform.Controls.Add(this.iosRadioButton);
            this.Platform.Controls.Add(this.androidRadioButton);
            this.Platform.Location = new System.Drawing.Point(589, 55);
            this.Platform.Name = "Platform";
            this.Platform.Size = new System.Drawing.Size(200, 100);
            this.Platform.TabIndex = 3;
            this.Platform.TabStop = false;
            this.Platform.Text = "Platform";
            this.Platform.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // iosRadioButton
            // 
            this.iosRadioButton.AutoSize = true;
            this.iosRadioButton.Location = new System.Drawing.Point(7, 43);
            this.iosRadioButton.Name = "iosRadioButton";
            this.iosRadioButton.Size = new System.Drawing.Size(43, 17);
            this.iosRadioButton.TabIndex = 3;
            this.iosRadioButton.Text = "IOS";
            this.iosRadioButton.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(447, 55);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Select";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.SelectCSV);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(447, 103);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Select";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.SelectPlayers);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(447, 149);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 6;
            this.button4.Text = "Select";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.SelectPurchases);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "CSV File:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Players:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Purchases:";
            // 
            // csvFile
            // 
            this.csvFile.Location = new System.Drawing.Point(83, 57);
            this.csvFile.Name = "csvFile";
            this.csvFile.Size = new System.Drawing.Size(343, 20);
            this.csvFile.TabIndex = 10;
            // 
            // playersFile
            // 
            this.playersFile.Location = new System.Drawing.Point(83, 105);
            this.playersFile.Name = "playersFile";
            this.playersFile.Size = new System.Drawing.Size(343, 20);
            this.playersFile.TabIndex = 11;
            // 
            // purchasesFile
            // 
            this.purchasesFile.Location = new System.Drawing.Point(83, 151);
            this.purchasesFile.Name = "purchasesFile";
            this.purchasesFile.Size = new System.Drawing.Size(343, 20);
            this.purchasesFile.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.commaRadioButton);
            this.groupBox1.Controls.Add(this.lineRadioButton);
            this.groupBox1.Controls.Add(this.semicolonRadioButton);
            this.groupBox1.Location = new System.Drawing.Point(589, 162);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Splitter";
            // 
            // commaRadioButton
            // 
            this.commaRadioButton.AutoSize = true;
            this.commaRadioButton.Location = new System.Drawing.Point(6, 68);
            this.commaRadioButton.Name = "commaRadioButton";
            this.commaRadioButton.Size = new System.Drawing.Size(60, 17);
            this.commaRadioButton.TabIndex = 2;
            this.commaRadioButton.Text = "Comma";
            this.commaRadioButton.UseVisualStyleBackColor = true;
            // 
            // lineRadioButton
            // 
            this.lineRadioButton.AutoSize = true;
            this.lineRadioButton.Checked = true;
            this.lineRadioButton.Location = new System.Drawing.Point(6, 44);
            this.lineRadioButton.Name = "lineRadioButton";
            this.lineRadioButton.Size = new System.Drawing.Size(45, 17);
            this.lineRadioButton.TabIndex = 1;
            this.lineRadioButton.TabStop = true;
            this.lineRadioButton.Text = "Line";
            this.lineRadioButton.UseVisualStyleBackColor = true;
            // 
            // semicolonRadioButton
            // 
            this.semicolonRadioButton.AutoSize = true;
            this.semicolonRadioButton.Location = new System.Drawing.Point(7, 20);
            this.semicolonRadioButton.Name = "semicolonRadioButton";
            this.semicolonRadioButton.Size = new System.Drawing.Size(74, 17);
            this.semicolonRadioButton.TabIndex = 0;
            this.semicolonRadioButton.Text = "Semicolon";
            this.semicolonRadioButton.UseVisualStyleBackColor = true;
            // 
            // fbRadioButton
            // 
            this.fbRadioButton.AutoSize = true;
            this.fbRadioButton.Location = new System.Drawing.Point(6, 67);
            this.fbRadioButton.Name = "fbRadioButton";
            this.fbRadioButton.Size = new System.Drawing.Size(93, 17);
            this.fbRadioButton.TabIndex = 4;
            this.fbRadioButton.TabStop = true;
            this.fbRadioButton.Text = "fbRadioButton";
            this.fbRadioButton.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 321);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.purchasesFile);
            this.Controls.Add(this.playersFile);
            this.Controls.Add(this.csvFile);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.Platform);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Platform.ResumeLayout(false);
            this.Platform.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton androidRadioButton;
        private System.Windows.Forms.GroupBox Platform;
        private System.Windows.Forms.RadioButton iosRadioButton;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox csvFile;
        private System.Windows.Forms.TextBox playersFile;
        private System.Windows.Forms.TextBox purchasesFile;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton commaRadioButton;
        private System.Windows.Forms.RadioButton lineRadioButton;
        private System.Windows.Forms.RadioButton semicolonRadioButton;
        private System.Windows.Forms.RadioButton fbRadioButton;
    }
}

