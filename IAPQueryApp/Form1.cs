﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Apis.Auth.OAuth2;
using Google.Apis.AndroidPublisher.v2;
using Google.Apis.Services;
using System.Net;
using System.Collections.Specialized;
using System.Threading;

namespace IAPQueryApp
{
    public partial class Form1 : Form
    {
        static string connectionString =
        @"Server=wokaback.coqo3h11cro8.us-west-2.rds.amazonaws.com;" +
        "Database=WokaWoka;" +
        "User ID=PlayerMaster;" +
        "Password=K4XnWCL42U-_58j#;" +
        "connect timeout=600;";

        const string googlePlayStore = "GooglePlay";
        const string appleAppStore = "AppleAppStore";
        const string fbStore = "payment_id";

        public bool androidPlatform = false;
        public bool iosPlatform = false;
        public bool fbPlatform = false;
        public bool windowsPlatform = false;

        List<string> dataRows = new List<string>();
        List<string> resultRows = new List<string>();

        Dictionary<string, string> players = new Dictionary<string, string>();
        Dictionary<string, string> countries = new Dictionary<string, string>();

        int androidPayloadIndex = 2;
        int iosPayloadIndex = 3;
        int fbPayloadIndex = 2;
        int windowsPayloadIndex = 2;

        List<Task> TaskList = new List<Task>();

        int counter = 0;

        int numOfTasks;

        char spliter;

        string tempOpenLocation = "";

        DateTime dateStart;
        DateTime dateEnd;
        bool completed = false;

        public Form1()
        {
            InitializeComponent();
        }

        void SelectPlatformSpecificPurchases()
        {
            if (androidRadioButton.Checked)
            {
                androidPlatform = true;
                iosPlatform = false;
                fbPlatform = false;
            }
            else if (iosRadioButton.Checked)
            {
                androidPlatform = false;
                iosPlatform = true;
                fbPlatform = false;
            }
            else if (fbRadioButton.Checked)
            {
                androidPlatform = false;
                iosPlatform = false;
                fbPlatform = true;
            }
        }

        bool CheckPlatform(string row)
        {
            //ako se stikliraju vise platformi, proci ce ako red sadrzi bilo koju
            //za sada android i ios hocemo tako da ako je red bilo koja od te dve platforme, ulazi u search
            if (androidPlatform && CheckAndroid(row))
            {
                return true;
            }

            if (iosPlatform && CheckIOS(row))
            {
                return true;
            }

            if (fbPlatform && CheckFB(row))
            {
                return true;
            }

            return false;
        }

        bool CheckAndroid(string row)
        {
            return row.Contains(googlePlayStore);
        }

        bool CheckIOS(string row)
        {
            return row.Contains(appleAppStore);
        }

        bool CheckFB(string row)
        {
            return row.Contains(fbStore);
        }

        void SetSplitCharacter()
        {
            if (semicolonRadioButton.Checked)
            {
                spliter = ';';
            }
            else if (lineRadioButton.Checked)
            {
                spliter = '|';
            }
            if (commaRadioButton.Checked)
            {
                spliter = ',';
            }
        }

        void Clean()
        {
            dataRows = new List<string>();
            players = new Dictionary<string, string>();
            countries = new Dictionary<string, string>();
            resultRows = new List<string>();
            TaskList = new List<Task>();
        }

        private void Start(object sender, EventArgs e)
        {
            Clean();
            SetSplitCharacter();

            SelectPlatformSpecificPurchases();
            //izbacuje |_| kombinacije da ne bi smetale kod splitovanja texta po |
            //jos nisam odradio
            FixCSV();
            
            if (androidPlatform)
            {
                Android();
            }
            else if (iosPlatform)
            {
                IOS();
            }
            else if (fbPlatform)
            {
                FB();
            }

            dateEnd = DateTime.Now;
            Debug.WriteLine("DoneT");
        }

        void FixCSV()
        {
            //ovo su sve situacije koje sam pronasao u fajlovima
            //ako neko pronadje jos neku kombinaciju karaktera sa |_| samo nek doda ovde

            StreamReader sr = new StreamReader(csvFile.Text);
            List<string> fixedRFows = new List<string>();
            while (!sr.EndOfStream)
            {
                var text = sr.ReadLine();
                text = text.Replace("0|_|", "");
                text = text.Replace("-1|_|", "");
                text = text.Replace("-2|_|", "");
                text = text.Replace("-3|_|", "");
                text = text.Replace("|_|", "");
                fixedRFows.Add(text);
            }
            sr.Close();
            File.WriteAllLines(csvFile.Text, fixedRFows);
            fixedRFows.Clear();
            Debug.WriteLine("Fixed");
        }

        void Android()
        {
            ReadPlayers();

            ReadValidatedAndroid();

            ReadCSVAndroid();

            WriteCSV(dataRows);
        }

        void IOS()
        {
            ReadPlayers();

            ReadCSVIOS();

            Task.Run(async () =>
            {
                // Do any async anything you need here without worry
                ValidateIOSPurchases();
            }).GetAwaiter().GetResult();

            Task.Run(async () =>
            {
                // Do any async anything you need here without worry
                WaitForTasks();
            }).GetAwaiter().GetResult();

            WriteCSV(resultRows);
        }

        void FB()
        {
            ReadPlayers();

            ReadCSVFB();

            Task.Run(async () =>
            {
                // Do any async anything you need here without worry
                ValidateFBPurchases();
            }).GetAwaiter().GetResult();

            Task.Run(async () =>
            {
                // Do any async anything you need here without worry
                WaitForTasks();
            }).GetAwaiter().GetResult();

            WriteCSV(resultRows);
        }

        async void WaitForTasks()
        {
            while (!completed)
            {
                Debug.WriteLine("not completed");
                Thread.Sleep(10000);
            }
        }

        async void ValidateIOSPurchases()
        {
            foreach (string row in dataRows)
            {
                if ((counter % 100 == 0 || counter == dataRows.Count - 1) && counter != 0)
                {
                    await Task.WhenAll(TaskList.ToArray());
                    TaskList.Clear();
                }
                IOSValidation(row);
                
            }
            completed = true;
        }

        async void ValidateFBPurchases()
        {
            foreach (string row in dataRows)
            {
                if ((counter % 100 == 0 || counter == dataRows.Count - 1) && counter != 0)
                {
                    await Task.WhenAll(TaskList.ToArray());
                    TaskList.Clear();
                }
                FBValidation(row);

            }
            completed = true;
        }

        void ReadValidatedAndroid()
        {
            dateStart = DateTime.Now;
            Debug.WriteLine("started file");
            StreamReader sr = new StreamReader(purchasesFile.Text);

            while (!sr.EndOfStream)
            {
                var row = sr.ReadLine();

                var values = row.Split(',');
                if (values[0].Contains("GPA.")) values[0] = values[0].Replace("GPA.", "");
                if (!countries.ContainsKey(values[0]))
                    countries.Add(values[0], values[values.Length - 1]);
                else
                {
                    Debug.WriteLine("already in dictionary");
                }
            }
            sr.Close();
        }       
        
        string[] ParseLine(string row)
        {
            var values = row.Split(spliter);

            //dodaj posle checkbox da li da se parsira dodatno ovo
            //ovo je csv koji sam ja skinuo iz baze i on ima dodatne navodnike u toj koloni gde je payload
            //zato sam ih ovde rucno skidao da bi deserijalizacija prosla
            //obrati paznju samo na index

            //TO DO 
            //Promeni ovo ako se dodaju jos platformi
            int index = androidPlatform ? androidPayloadIndex : (iosPlatform ? iosPayloadIndex : (fbPlatform ? fbPayloadIndex : windowsPayloadIndex));

            Debug.WriteLine(values[2]);
            if (spliter == ';')
            {
                values[index] = values[index].Replace("\"\"", "\"");
                values[index] = values[index].Remove(0, 1);
                values[index] = values[index].Remove(values[index].Length - 1, 1);
            }
            return values;
        }

        string ValidateAndroid(string[] values)
        {
            try
            {
                Dictionary<string, object> res = JsonConvert.DeserializeObject<Dictionary<string, object>>(values[2]);
                string transaction = res["TransactionID"].ToString();
                if (transaction.Contains("GPA.")) transaction = transaction.Replace("GPA.", "");
                if (countries.ContainsKey(transaction))
                {
                    return countries[transaction];
                }
                else
                {
                    return string.Empty;
                }
            }
            catch
            {
                Debug.WriteLine("failed");
                return string.Empty;
            }
        }

        void IOSValidation(string row)
        {
            string token = "";
            
            try
            {
                //Dictionary<string, object> res = JsonConvert.DeserializeObject<Dictionary<string, object>>(row);
                //token = res["Payload"].ToString();
                //row.Replace("\"", "");

                var values = ParseLine(row);
                token = values[3];
                Dictionary<string, object> res = JsonConvert.DeserializeObject<Dictionary<string, object>>(token);
                token = res["Payload"].ToString();
            }
            catch
            {
                Debug.WriteLine("failed = " + row);
            }
            if (!string.IsNullOrEmpty(token))
            {
                //Debug.WriteLine("numOfTasks = " + numOfTasks);
                /*
                Action task = new Action(() =>              
                {

                    ValidateIOSInAppPurchaseValue(row, token);
                });
                    
                var LastTask = new Task(task);
                //LastTask.Start();
                TaskList.Add(LastTask);
                */
                Task task = Task.Run(() => ValidateIOSInAppPurchase(row, token));
                TaskList.Add(task);
                counter++;
                /*
                if ((counter % 100 == 0 || counter == dataRows.Count - 1) && counter != 0)
                {
                    await Task.WhenAll(TaskList.ToArray());
                    TaskList.Clear();
                }
                */
            }
        }

        void FBValidation(string row)
        {
            string token = "";
            string price = "0";
            var values = ParseLine(row);
            try
            {
                token = values[3]; //purchase id
                Dictionary<string, object> res = JsonConvert.DeserializeObject<Dictionary<string, object>>(token);
                if (res.ContainsKey("payment_id"))
                {
                    token = res["payment_id"].ToString();
                }
                if (res.ContainsKey("amount"))
                {
                    price = res["amount"].ToString();
                }
            }
            catch
            {
                Debug.WriteLine("failed = " + row);
            }
            if (!string.IsNullOrEmpty(token))
            {
                Task task = Task.Run(() => ValidateFBInAppPurchase(values[0], token, price));
                TaskList.Add(task);
                counter++;
                /*
                if ((counter % 100 == 0 || counter == dataRows.Count - 1) && counter != 0)
                {
                    await Task.WhenAll(TaskList.ToArray());
                    TaskList.Clear();
                }
                */
            }
        }

        void ReadCSVAndroid()
        {
            dataRows = new List<string>();
            dateStart = DateTime.Now;
            Debug.WriteLine("started file ");
            StreamReader sr = new StreamReader(csvFile.Text);

            while (!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                if (!CheckPlatform(line))
                {
                    continue;
                }

                var values = ParseLine(line);

                string fbid = GetUserFBID(values);
                string country = string.Empty;



                if (!string.IsNullOrEmpty(fbid))
                {
                    country = ValidateAndroid(values);
                }
                
                if (!string.IsNullOrEmpty(fbid) && !string.IsNullOrEmpty(country))
                {
                    string price = GetPurchaseCostAndroid(values);
                    dataRows.Add(fbid + ';' + price + ';' + country);
                }
                
            }
            sr.Close();
        }

        void ReadCSVIOS()
        {
            dataRows = new List<string>();
            dateStart = DateTime.Now;
            Debug.WriteLine("started file ");
            StreamReader sr = new StreamReader(csvFile.Text);

            while (!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                if (!CheckPlatform(line))
                {
                    continue;
                }

                var values = ParseLine(line);

                string fbid = GetUserFBID(values);

                //no country for now

                if (!string.IsNullOrEmpty(fbid))
                {
                    dataRows.Add(fbid + spliter + line);
                }

            }
            sr.Close();
        }

        void ReadCSVFB()
        {
            dataRows = new List<string>();
            dateStart = DateTime.Now;
            Debug.WriteLine("started file ");
            StreamReader sr = new StreamReader(csvFile.Text);

            while (!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                if (!CheckPlatform(line))
                {
                    continue;
                }

                var values = ParseLine(line);

                string fbid = GetUserFBID(values);

                //no country for now

                if (!string.IsNullOrEmpty(fbid))
                {
                    dataRows.Add(fbid + spliter + line);
                }

            }
            sr.Close();
        }

        void ReadPlayers()
        {
            //2663021
            dateStart = DateTime.Now;
            Debug.WriteLine("started file ");
            //D:\VisualStudioProjects\IAPQueryApp\IAPQueryApp\bin\Debug - ovo je folder
            StreamReader sr = new StreamReader(playersFile.Text);

            while (!sr.EndOfStream)
            {
                var line = sr.ReadLine();
                var values = line.Split(';');
                if(!players.ContainsKey(values[0]))
                    players.Add(values[0], values[1]);
            }
            sr.Close();
        }

        string GetUserFBID(string[] values)
        {
            if (players.ContainsKey(values[1])) //UserID
            {
                return players[values[1]];
            }
            else return string.Empty;
        }

        void WriteCSV(List<string> dataRows)
        {
            Debug.WriteLine("started writing; counter =  " + counter);

            //File.WriteAllLines("iosValidated.csv", dataRows);
            OpenSaveWIndow(dataRows);
        }

        void OpenSaveWIndow(List<string> dataRows)
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    // Code to write the stream goes here.
                    myStream.Close();
                    File.WriteAllLines(saveFileDialog1.FileName, dataRows);
                    
                }
            }
        }

        public async Task<float> ValidateIOSInAppPurchase(string row, string token)
        {

            try
            {
                numOfTasks++;
                string iOSStoreURL = "https://buy.itunes.apple.com/verifyReceipt";
                Dictionary<string, object> dicTemp = new Dictionary<string, object>();
                dicTemp.Add("receipt-data", token);
                string jsonData = JsonConvert.SerializeObject(dicTemp);

                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage wcfResponse = await httpClient.PostAsync(iOSStoreURL, new StringContent(jsonData, Encoding.UTF8, "application/json"));

                string result = await wcfResponse.Content.ReadAsStringAsync();
                //Debug.WriteLine(result);

                string price = GetPurchaseCostIOS(result);

                if (!string.IsNullOrEmpty(price))
                {
                    var values = row.Split(spliter);
                    resultRows.Add(values[0] + ';' + price);
                }

                numOfTasks--;

                Debug.WriteLine("numOfTasks-- = " + numOfTasks);
                return 0f;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                numOfTasks--;
                Debug.WriteLine("catch numOfTasks-- = " + numOfTasks);
                return 0;
            }
        }

        public async Task<float> ValidateFBInAppPurchase(string fbid, string purchaseID, string price)
        {
            //https://graph.facebook.com/v2.12/1607344259379303?fields=country&access_token=1597628043840846|hGgY4b12zGr6t4AOZ7GQz2XR1SQ
            try
            {
                numOfTasks++;
                string fbURL = "https://graph.facebook.com/v2.12/" + purchaseID + "?fields=country&access_token=1597628043840846|hGgY4b12zGr6t4AOZ7GQz2XR1SQ";
                Dictionary<string, object> dicTemp = new Dictionary<string, object>();

                HttpClient httpClient = new HttpClient();
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage wcfResponse = await httpClient.GetAsync(fbURL);

                string result = await wcfResponse.Content.ReadAsStringAsync();
                //Debug.WriteLine(result);

                string country = GetCountryFB(result);

                resultRows.Add(fbid + ';' + price + ';' + country);

                numOfTasks--;

                Debug.WriteLine("numOfTasks-- = " + numOfTasks);
                return 0f;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                numOfTasks--;
                Debug.WriteLine("catch numOfTasks-- = " + numOfTasks);
                return 0;
            }
        }


        public string ValidateGoogleInAppPurchase()
        {
            ServiceAccountCredential credential = new ServiceAccountCredential(
           new ServiceAccountCredential.Initializer("webserviceapi@api-project-458131927214.iam.gserviceaccount.com")
           {
               Scopes = new[] { "https://www.googleapis.com/auth/androidpublisher" },
               }.FromPrivateKey("-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDEp4dlU4BldIAR\nzHtOiI+KuXH42BQUjBXEy9x8b7H+UzUcsgQnJSEKRRJyK4bzooIlhEBmK+R0B2HA\nUJyY1sT8nOwzDwn8SPH4+eMvVdnqp96jcx7huYFwWjrETU7OcOlbvIA12DOA+kND\nsEG0jqYcC73N/MsMoBn/icOUWHIHXaa9EJim+yFxkA51cfHoIj5XqVI+IeUxiJlM\noLXG5tPVYctQ6IBbFynQsRQ5zjK5LGVP76Ufbya7UL5A97QAI3WV8vh3G5TgOOSi\nccddpgvD5vpDV/94VOq5BOqPu9xKTOpmfZ6wfxXNMhAWzjY9xDXhVqbrM0gt5uGJ\nUJ3Vs/c9AgMBAAECggEAXzIt89n5JKPBv1v6MOkge2oMBwcqqUuq2gpJoJ89Tb0f\nz9VI914c4WjQZh42BMidMeYUeqEyxQwxwLQFKmOFFjgsiwbzsYHqSLlGxcpMZDCB\nMSRwRZrUUvwKyK4XwQ2dmX/lnKsdO4lqeGDhXbNUK+1vI0EFVgZGr5IT8ELkaDWO\nzw2kDTYXztKkWGiH8F0W5hbkaG6XgAOn7YbXG2tzxo6qSmLZ0yiSdLTjL9dk5RhU\nTo+weBvIGEbT8ZgJvj5wHnQO+7SKTpZRYJ/uI9iPSYPVZGbaZfU6PuJLI0ra9TyS\nctPo8Cx4lS3NUav/4y+N/wjat3Qll7xeGgG4tvez7QKBgQDxIETfO//oaeCIsj9a\n/8gUloHPTV5xqcyi8/cjbYcfNmNIacb2MR+iuqN7YSwIBSfQRBKUdq+hNeVIdCEu\nJieyj1NEH/eAcxkdBYeJHcmw/VEgpHXgnfx4a2W+WLH29Bg6BCOFmi0PxowJLbeF\nWt7a5PUXySdTOteE6rWNZx5juwKBgQDQyPzsGDaAAjgUJUwy45Ec/MOoSnZbeHlN\nSLP9/fnWKD38bUIvw1JVy9xb2BqxVAXtMl7YGD2BsiH0Z5/TfAGiJOgbb68Eh1QN\noZGL+7exyXDgiL9BJi8pfhsqhhYFpUoI8znhBUyBjelxPpjQDoPmhCg4aMR+jIPc\nOaS7x2qVZwKBgQCOaJKEjXkcmLuFwFBuvDY9Ao70Eiu/UZuMei3x/JQbORH2CJL7\ngBPBFh3Llp5ctj7HYAzhJh+gRKZ/3iZijSJQ8RkqWP7iHBgu7b8YmkEuiM3LS5Za\n7LPZowRaISQqSAuu6Bbapy4J7SdznE6RMhQAj8y5GZJiM5Rp6f0D4G0wVQKBgAqI\nOQoH5qfqdL2/a+qdRDK8JXqV1waiOyxE4J+Nck71VLspH8+dAMhHMmSHsEEFxg7D\nHbm071VdkQztNUjL1qFBT1wi9Cc8VGcjoV34YrnbOZB6FXvJ21Myg1hPz0iVn/h3\nnJHXXJakfAQ0bi0ArnKwc/70egUQPY6ccczvMFc9AoGAFH4r/FTeYiv3ETx8pNMz\naxk07vG71j3wzfOz2o2wBpp3SS4MdkxYBYPMn5tboT5KMKpOtKhs6vdnH4TEFZ/f\n0Iz4w0k58nR8hpy/QQhgqlqOWqaOHHJhqZbCtDMWU1WAs3aQPoSJJA4pLnyU5Z1H\ngScDeu8BW+9J779ul5V7jLQ=\n-----END PRIVATE KEY-----\n"));
           //}.FromPrivateKey("nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDEp4dlU4BldIAR\nzHtOiI+KuXH42BQUjBXEy9x8b7H+UzUcsgQnJSEKRRJyK4bzooIlhEBmK+R0B2HA\nUJyY1sT8nOwzDwn8SPH4+eMvVdnqp96jcx7huYFwWjrETU7OcOlbvIA12DOA+kND\nsEG0jqYcC73N/MsMoBn/icOUWHIHXaa9EJim+yFxkA51cfHoIj5XqVI+IeUxiJlM\noLXG5tPVYctQ6IBbFynQsRQ5zjK5LGVP76Ufbya7UL5A97QAI3WV8vh3G5TgOOSi\nccddpgvD5vpDV/94VOq5BOqPu9xKTOpmfZ6wfxXNMhAWzjY9xDXhVqbrM0gt5uGJ\nUJ3Vs/c9AgMBAAECggEAXzIt89n5JKPBv1v6MOkge2oMBwcqqUuq2gpJoJ89Tb0f\nz9VI914c4WjQZh42BMidMeYUeqEyxQwxwLQFKmOFFjgsiwbzsYHqSLlGxcpMZDCB\nMSRwRZrUUvwKyK4XwQ2dmX/lnKsdO4lqeGDhXbNUK+1vI0EFVgZGr5IT8ELkaDWO\nzw2kDTYXztKkWGiH8F0W5hbkaG6XgAOn7YbXG2tzxo6qSmLZ0yiSdLTjL9dk5RhU\nTo+weBvIGEbT8ZgJvj5wHnQO+7SKTpZRYJ/uI9iPSYPVZGbaZfU6PuJLI0ra9TyS\nctPo8Cx4lS3NUav/4y+N/wjat3Qll7xeGgG4tvez7QKBgQDxIETfO//oaeCIsj9a\n/8gUloHPTV5xqcyi8/cjbYcfNmNIacb2MR+iuqN7YSwIBSfQRBKUdq+hNeVIdCEu\nJieyj1NEH/eAcxkdBYeJHcmw/VEgpHXgnfx4a2W+WLH29Bg6BCOFmi0PxowJLbeF\nWt7a5PUXySdTOteE6rWNZx5juwKBgQDQyPzsGDaAAjgUJUwy45Ec/MOoSnZbeHlN\nSLP9/fnWKD38bUIvw1JVy9xb2BqxVAXtMl7YGD2BsiH0Z5/TfAGiJOgbb68Eh1QN\noZGL+7exyXDgiL9BJi8pfhsqhhYFpUoI8znhBUyBjelxPpjQDoPmhCg4aMR+jIPc\nOaS7x2qVZwKBgQCOaJKEjXkcmLuFwFBuvDY9Ao70Eiu/UZuMei3x/JQbORH2CJL7\ngBPBFh3Llp5ctj7HYAzhJh+gRKZ/3iZijSJQ8RkqWP7iHBgu7b8YmkEuiM3LS5Za\n7LPZowRaISQqSAuu6Bbapy4J7SdznE6RMhQAj8y5GZJiM5Rp6f0D4G0wVQKBgAqI\nOQoH5qfqdL2/a+qdRDK8JXqV1waiOyxE4J+Nck71VLspH8+dAMhHMmSHsEEFxg7D\nHbm071VdkQztNUjL1qFBT1wi9Cc8VGcjoV34YrnbOZB6FXvJ21Myg1hPz0iVn/h3\nnJHXXJakfAQ0bi0ArnKwc/70egUQPY6ccczvMFc9AoGAFH4r/FTeYiv3ETx8pNMz\naxk07vG71j3wzfOz2o2wBpp3SS4MdkxYBYPMn5tboT5KMKpOtKhs6vdnH4TEFZ/f\n0Iz4w0k58nR8hpy/QQhgqlqOWqaOHHJhqZbCtDMWU1WAs3aQPoSJJA4pLnyU5Z1H\ngScDeu8BW+9J779ul5V7jLQ="));
            // Create the service.
            var service = new AndroidPublisherService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential
            });

            try
            {
                /*                  https://www.googleapis.com/androidpublisher/v2/applications/{0}/purchases/products/{1}/tokens/rojeslcdyyiapnqcynkjyyjh?access_token=your_access_token
                var url = string.Format("https://www.googleapis.com/androidpublisher/v2/applications/{0}/purchases/products/{1}/tokens/{2}", "com.TwoDesperados.WokaWoka", "coinpackage2", "polfohoinfemblmjdnnnolno.AO-J1OxFXLkCZ-2eStnNiLPz3CadBYPhUe_6voXTzzppfC8BVSWWkTP_pxT4zp9yYajbc33J7MOTa_xMXiNOqZ43Q-yPLFDHi-aFdZCrcSZ_GI2n0Nq0oHZUPEgQ0fZhCOYmtwYS2IE6");
                var request = HttpWebRequest.Create(url);
                request.Method = "GET";
                request.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //Debug.WriteLine(((HttpWebResponse)response).StatusDescription);
                Debug.WriteLine("done");*/

                
                var data = service.Purchases.Products.Get(
                            "com.TwoDesperados.WokaWoka",
                            "coinpackage3",
                            "kinnkohppjjpfagpidpmbala.AO-J1Ox_A3wlCgfPWwJ922WajjQPW2-zgzl1RTngyRxyfMpTuGmOFIyI5a_ZsxVgAwZnZ7zZlsqmbFwHKPMXwj7oy_Hi8m2pHl6uxKhebli3IKovL6cVAWsBsBB0YIyfXsFs6VXBQiuw")
                            .Execute();

                Debug.WriteLine("data: " + data.PurchaseState + " - " + data.OrderId + " - " + data.DeveloperPayload);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ex " + ex.Message);
            }

            return "end";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //write
            WriteCSV(resultRows);
        }

        private double GetValueByPackage(string packageID)
        {
            switch (packageID)
            {
                case "coinpackage1":
                    return 0.99;
                case "coinpackage2":
                    return 4.49;
                case "coinpackage3":
                    return 8.99;
                case "coinpackage4":
                    return 19.99;
                case "coinpackage5":
                    return 39.99;
                case "coinpackage6":
                    return 16.99;
                case "coinpackage7":
                    return 24.99;
                case "coinpackage8":
                    return 72.99;
                case "specialoffer_1":
                    return 0.99;
                case "specialoffer_2":
                    return 0.99;
                case "supersale1":
                    return 2.2;
                case "supersale2":
                    return 1.99;
                case "supersale3":
                    return 2.2;
                case "valentinesoffer":
                    return 2.2;
                default:
                    return 0;
            }
        }

        string GetPurchaseCostAndroid(string[] values)
        {
            Dictionary<string, object> res = JsonConvert.DeserializeObject<Dictionary<string, object>>(values[2]);
            string price = "";

            if (res.ContainsKey("Payload"))
            {
                Dictionary<string, object> res2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(res["Payload"].ToString());
                if (res2.ContainsKey("json"))
                {
                    Dictionary<string, object> res3 = JsonConvert.DeserializeObject<Dictionary<string, object>>(res2["json"].ToString());
                    if (res3.ContainsKey("productId"))
                    {
                        price = GetValueByPackage(res3["productId"].ToString()).ToString();
                    }
                }
            }
            return price;
        }

        string GetPurchaseCostIOS(string result)
        {
            string price = string.Empty;
            try
            {
                Dictionary<string, object> res = JsonConvert.DeserializeObject<Dictionary<string, object>>(result);
                if (res.ContainsKey("status") && res["status"].ToString().Equals("0"))
                {
                    if (res.ContainsKey("receipt"))
                    {
                        Dictionary<string, object> res2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(res["receipt"].ToString());
                        if (res2.ContainsKey("in_app"))
                        {
                            List<Dictionary<string, object>> res3 = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(res2["in_app"].ToString());
                            if (res3[0].ContainsKey("product_id"))
                            {
                                price = GetValueByPackage(res3[0]["product_id"].ToString()).ToString();
                            }

                        }

                    }

                }
                else
                {
                    Debug.WriteLine("status not valid");
                }
            }
            catch
            {
            }
            return price;
        }

        string GetCountryFB(string data)
        {
            Dictionary<string, object> dicData = JsonConvert.DeserializeObject<Dictionary<string, object>>(data);
            if (dicData.ContainsKey("country"))
            {
                return dicData["country"].ToString();
            }
            return "0";
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void SelectPlayers(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = tempOpenLocation;
            openFileDialog1.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    playersFile.Text = openFileDialog1.FileName;
                    tempOpenLocation = openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void SelectPurchases(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = tempOpenLocation;
            openFileDialog1.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    purchasesFile.Text = openFileDialog1.FileName;
                    tempOpenLocation = openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void SelectCSV(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = tempOpenLocation;
            openFileDialog1.Filter = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    csvFile.Text = openFileDialog1.FileName;
                    tempOpenLocation = openFileDialog1.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }
    }
}
